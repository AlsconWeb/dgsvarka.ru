# WP Themes dgsvarka

Theme Block Screenshots

#### Шапка главный баннер (Name: header_main_banner)

![Шапка главный баннер](http://image-git.alscon-clients.com/dgsvarka/header_main_banner.png)

##### Fields
- Картинка баннера (Name: banner_image)
- Логотип (Name: Logo)
- Логотип текст (Name: Logo_text)
- Время работы (Name: time_work)
- Иконка (Name: icon)
- Текст контактов (Name: text_contact)
- Главный текст баннера (Name: main_text)
- Блок Иконок (Name: block_icon)
  - Иконка (Name: icon)
  - Текст  (Name: text_icon_block)
- Форма обратной связи (Name: form_shordcode)

#### Виды услуг (Name: types_of_services)

![Виды услуг](http://image-git.alscon-clients.com/dgsvarka/types_of_services.png)

##### Fields
- Заголовок (Name: title)
- Услуги (Name: services_item)
  - Картинка (Name: image)
  - Виды услуг (Name: services_item_type)
- Текст после блока услуг заголовок (Name: text_after_block)
- Текст после блока услуг (Name: text_after_block_text)

#### Отрасли (Name: grown_block)
![Отрасли](http://image-git.alscon-clients.com/dgsvarka/grown_block.png)

##### Fields
- Заголовок (Name: title)
- Отрасли (Name: grown_item)
- Блок что мы варим (Name: block_welding)
- Виды металов (Name: weldings_item)
  - Метал (Name: item)

#### Блок наши цены (Name: price_block)
![Блок наши цены](http://image-git.alscon-clients.com/dgsvarka/price_block.png)

##### Fields
- Цены (Name: price_menu)
  - Меню (Name: item)
- Цены Таблицы (Name: price_item_tables)
  - Таблица (Name: table_item)

#### Блок Консультации мастера (Name: block_master_form)
![Блок Консультации мастера](http://image-git.alscon-clients.com/dgsvarka/block_call_master.png)

##### Fields
- Заголовок (Name: title)
- Под заголовок (Name: sub_title)
- Форма (Name: form_shordcode)
- Картинка (Name: Image)

#### Блок стоимость по фото и чертижу (Name: block_master_form)
![Блок стоимость по фото и чертижу](http://image-git.alscon-clients.com/dgsvarka/block_master_form.png)

##### Fields
- Заголовок (Name: title)
- Под заголовок (Name: sub_title)
- Форма (Name: form)
- Бекграунд блока (Name: background)

#### Блок нет света (Name: block_gen)
![Блок нет света](http://image-git.alscon-clients.com/dgsvarka/block_gen.png)

##### Fields
- Картинка (Name: image)
- Заголовок (Name: title)
- Под заголовок (Name: sub_title)

#### Блок преимущества (Name: block_advantages)
![Блок преимущества](http://image-git.alscon-clients.com/dgsvarka/block_advantages.png)

##### Fields
- Заголовок (Name: title)
- Преимущества (Name: advantages_item)
  - Иконка (Name: icons)
  - Заголовок (Name: title)
  - Текст (Name: text)


#### Блок производства (Name: block_manufacture)
![Блок производства](http://image-git.alscon-clients.com/dgsvarka/block_manufacture.png)

##### Fields
- Заголовок (Name: title)
- Под заголовок (Name: sub_title)
- Картинка (Name: image)

#### Блок как мы работаем (Name: block_how_we_work)
![Блок как мы работаем](http://image-git.alscon-clients.com/dgsvarka/block_how_we_work.png)

##### Fields
- Заголовок (Name: title)
- Как мы работаем (Name: how_we_work)
  - Текст (Name: text)
- Заголовок время работы (Name: title_time_work)
- Под заголовок время работы (Name: sub_title_time_work)
- Картинка (Name: image)
- Форма (Name: form)

#### Блок наши работы (Name: block_portfolio)
![Блок наши работы](http://image-git.alscon-clients.com/dgsvarka/block_portfolio.png)

##### Fields
- Заголовок (Name: title)
- Портфолио (Name: portfolio)
  - Картинка (Name: image)

#### Блок отзывы (Name: block_reviews)
![Блок отзывы](http://image-git.alscon-clients.com/dgsvarka/block_reviews.png)

##### Fields
- Заголовок (Name: title)
- Отзвы (Name: reviews)
  - Заголовок (Name: title)
  - Текст (Name: text)
  - Автор отзыва (Name: author_reviews)


#### Блок наша команда (Name: block_our_team)
![Блок наша команда](http://image-git.alscon-clients.com/dgsvarka/block_our_team.png)

##### Fields
- Заголовк (Name: title)
- Текст (Name: text)

#### Блок часто задаваемые вопросы (Name: block_faq)
![Блок часто задаваемые вопросы](http://image-git.alscon-clients.com/dgsvarka/block_faq.png)

##### Fields
- Заголовок (Name: title)
- Вопросы (Name: faq_item)
  - Вопрос (Name: question)
  - Ответ (Name: response)
- Кнопка задать вопрос (Name: btn_questions)

#### Блок контакты (Name: block_contact)
![Блок контакты](http://image-git.alscon-clients.com/dgsvarka/block_contact.png)

##### Fields
- Заголовок (Name: title)
- Текст контактов (Name: text)
- Шорткод формы (Name: form)

#### Блок вызвать мастера (Name: block_call_master)
![Блок вызвать мастера](http://image-git.alscon-clients.com/dgsvarka/block_call_master.png)

##### Fields
- Заголовок (Name: title)
- Шорткод формы (Name: forma)
- Ссылка на WhatsApp (Name: link_to_whatsapp)
- Иконка (Name: icon)
- Текст (Name: text)
- Заголовок адреса (Name: adress_title)
- Текст (Name: text_rep)
  - Строки текста (Name: item_text)

#### Хедер страниц услуги (Name: block_call_master)
![Хедер страниц услуги](http://image-git.alscon-clients.com/dgsvarka/header_service.png)

##### Fields
- Слайдер (Name: slider)
  - Картинка (Name: image)
- Цена (Name: price)
- Текст (Name: text)
- Описание Услуги (Name: description_service)

#### Блок с текстом (Name: block_text)
![Блок с текстом](http://image-git.alscon-clients.com/dgsvarka/block_text.png)

##### Fields
- Заголовок (Name: title)
- Текст (Name: text)

#### Блок Формы с текстом с фиксированной  позицией в низу(Name: block_fix_position_form)
![Блок Формы с текстом с фиксированной  позицией в низу](http://image-git.alscon-clients.com/dgsvarka/block_fix_position_form.png)

##### Fields
- Текст (Name: text)
- Шорт Код Формы (Name: short_code)
- Цвет фона Блока (Name: color_bg_block)
- Цвет Текста (Name: color_text)

### Version: 1.1.1
* Update ACF Options
* Add options (on\off) filters in inner page 
* edit styles

#### Version: 1.1.0
* Update ACF PRO 5.8.1
* Add Capture the lead with a fixed position at the bottom of the site 
* Fix meta title 
* Add 404 template
* Added check for the presence of the menu and its output 
