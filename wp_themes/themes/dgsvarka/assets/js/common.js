jQuery(document).ready(function ($) {
	$('p').each(function(){
		if($(this).text().length <= 1){
			$(this).remove();
		}
		
	});
	$(".fancy").fancybox({

	});
	$("input[name='phone']").mask("+7 (999) 99-99-999");
	var isDragging = false;
	$("#show_calc")
		.mousedown(function () {
			$(window).mousemove(function () {
				isDragging = true;
				$(window).unbind("mousemove");
			});
		})
		.mouseup(function () {
			var wasDragging = isDragging;
			isDragging = false;
			$(window).unbind("mousemove");
			if (!wasDragging) {
				$(".calc_block.calc_block2").toggleClass("active");
				$("#show_calc").toggleClass("active2");
			}
		});

	$(window).scroll(function () {
		// var footer = jQuery('footer').offset().top;
		// console.log("footer", footer);
		console.log('ScrotllTop', jQuery(this).scrollTop());
		if (jQuery(window).scrollTop() + jQuery(window).height() >= jQuery(document).height() - 84) {
			console.log("IN");
			$('.fix_bottom').css({
				"bottom": "84px"
			});
		} else {
			$('.fix_bottom').css({
				"bottom": "0px"
			});
		}
	});


	function tablePriceCheck() {
		var i;
		$("#scrl4 .tabs_block .tabs_content .tc").each(function (index, key) {
			var id = $(this).attr("id");
			var kol = 0;
			$("#" + id + " table tbody tr td:nth-child(3)").each(function (index, key) {
				if ($(this).html() != "")
					kol++;
			})
			if (kol == 0) {
				$("#" + id + " table tbody tr td:nth-child(3)").hide();
				$("#" + id + " table thead tr td:nth-child(3)").hide();
			}
		});
	}

	tablePriceCheck();



	$("#scrl4 .tabs_block .tabs_content table tbody tr").on("click", function () {
		if ($(this).find("td:last-child a").length > 0)
			window.location.href = $(this).find("td:last-child a").attr("href");
	});


	$("#scrl8 .items .item .open_close_desc").on("click", function () {
		var $this = $(this);
		$this.parent().find(".review_desc").toggleClass("active");
		if ($this.parent().find(".review_desc").hasClass("active"))
			$this.html("Скрыть");
		else
			$this.html("Читать полностью");
	});

	var menu_pos = $("#menu").offset().top;

	if ($(window).scrollTop() > menu_pos)
		$("#menu").addClass("fixed");

	$(window).on("scroll", function () {
		if ($(window).scrollTop() > menu_pos)
			$("#menu").addClass("fixed");
		else
			$("#menu").removeClass("fixed");
	});

	var slider_works = $('.bxslider_works').owlCarousel({
		loop: true,
		margin: 10,
		nav: false,
		rows: true,
		rowsCount: 2,
		responsive: {
			0: {
				items: 1
			},
			480: {
				items: 3
			}
		}
	});

	var slider_team = $('.bxslider_team').owlCarousel({
		loop: true,
		margin: 20,
		nav: false,
		responsive: {
			0: {
				items: 1
			},
			480: {
				items: 2
			},
			720: {
				items: 4
			}
		}
	});

	var slider_t = $('.bxslider_testimonials').owlCarousel({
		loop: true,
		margin: 20,
		nav: false,
		responsive: {
			0: {
				items: 1
			},
			480: {
				items: 3
			}
		}
	});

	if ($(window).width() < 600) {
		$('header .mid .items').owlCarousel({
			loop: true,
			margin: 20,
			nav: false,
			responsive: {
				0: {
					items: 1
				}
			}
		});
	} else {
		$('header .mid .items').show();
	}

	$("#scrl6 .slider_arrows .prev").on("click", function () {
		slider_works.trigger('prev.owl.carousel');
	})

	$("#scrl6 .slider_arrows .next").on("click", function () {
		slider_works.trigger('next.owl.carousel');
	})

	$("#team .slider_arrows .prev").on("click", function () {
		slider_team.trigger('prev.owl.carousel');
	})

	$("#team .slider_arrows .next").on("click", function () {
		slider_team.trigger('next.owl.carousel');
	})

	$("#scrl8 .slider_arrows .prev").on("click", function () {
		slider_t.trigger('prev.owl.carousel');
	})

	$("#scrl8 .slider_arrows .next").on("click", function () {
		slider_t.trigger('next.owl.carousel');
	})

	$("#faq .faq_item:first-child").addClass("active");

	$("#faq .faq_item").on("click", function () {
		$("#faq .faq_item").removeClass("active");
		$(this).addClass("active");
	});

	$("#scrl_b").on("click", function () {
		$('html,body').animate({
			scrollTop: $("#menu").offset().top
		}, 1000);
	});

	$('a[href*=#]:not([href=#])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
	});
	$("#scrl4 .tabs_block .tabs_links ul li a").on("click", function (e) {
		e.preventDefault();
		var id = $(this).attr("data-id");
		$("#scrl4 .tabs_block .tabs_links ul li a").removeClass("active");
		$(this).addClass("active");
		$("#scrl4 .tabs_block .tabs_content .tc").hide();
		$("#tc_" + id).show();
	});

	$("#scrl4 .tabs_block .tabs_links ul li:first-child a").trigger("click");

	$("#form3 .form .file_block a").on("click", function () {
		$(this).parent().find("input").trigger("click");
	});

	$("#form3 .form .file_block input").on("change", function () {
		var fname = $(this).val().split('\\').pop();
		$("#form3 .form .file_block span.filename").html(fname);
		$("#form3 .form .file_block span.filename").addClass("active");
	});

	$("#form3 .form .file_block span.filename").on("click", function () {
		$("#form3 .form .file_block span.filename").html("");
		$("#form3 .form .file_block span.filename").removeClass("active");
		$("#form3 .form .file_block input").val('');
	});

	//    $(".open_close_form a").on("click", function(){
	//        $(this).hide();
	//        $(this).parent().parent().find(".open_close").show(100);
	//    });

	$(document).on("scroll", onScroll);

	//smoothscroll
	$('.menu a[href^="#"]').on('click', function (e) {
		e.preventDefault();
		$(document).off("scroll");

		$('a').each(function () {
			$(this).removeClass('active');
		})
		$(this).addClass('active');

		var target = this.hash,
			menu = target;
		$target = $(target);
		$('html, body').stop().animate({
			'scrollTop': $target.offset().top + 2
		}, 500, 'swing', function () {
			//            window.location.hash = target;
			$(document).on("scroll", onScroll);
		});
	});
	if ($('#scrl2').length) {
		function onScroll(event) {
			var scrollPos = $(document).scrollTop();
			$('#menu li a.scrl').each(function () {
				var currLink = $(this);
				var refElement = $(currLink.attr("href"));
				if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
					$('#menu li a').removeClass("active");
					currLink.addClass("active");
				} else {
					currLink.removeClass("active");
				}
			});
		}
	}

	function checkScroll() {
		if ($(window).scrollTop() > 900) {
			$('#scroll-to-top').fadeIn();
		} else {
			$('#scroll-to-top').fadeOut();
		}
	}
	$(".calc_block form").validate({
		rules: {
			phone: "required"
		},
		submitHandler: function () {
			$('.calc_block form p.status').show().text("РћС‚РїСЂР°РІРєР° РґР°РЅРЅС‹С…...");
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: "/ajax/selects.php",
				data: {
					'action': 'send',
					'phone': $('.calc_block form input[name="phone"]').val(),
					'name': $('.calc_block form input[name="name"]').val(),
					'ch_gen': $('.calc_block form input[name="ch_gen"]').val(),
					'razmer': $('.calc_block .razmer input').val(),
					'usluga': $('.calc_block .usluga select').val(),
					'vid': $('.calc_block .vid_rabot select option:selected').html(),
					'viezd': $('.calc_block .viezd_block a.active').html(),
					'price': $('.calc_block .calc_price span').html()
				},
				success: function (data) {
					$('.calc_block form p.status').text(data.message);
				}
			});
		}
	});

	function reCalc() {
		var sum = 0;
		if ($(".calc_block .usluga select").val().length > 0) {
			var price1 = $(".calc_block .usluga select option:selected").attr("data-price");
			var price2 = $(".calc_block .usluga select option:selected").attr("data-price2");
			sum += parseInt(parseInt(price1) * parseInt($(".calc_block .razmer input").val()));
			if ($('#ch_gen').is(':checked'))
				sum += parseInt($("#ch_gen").attr("data-price"));
			if ($(".calc_block .viezd_block a:first-child").hasClass("active"))
				sum += parseInt(price2);
		}
		$(".calc_block .calc_price span").html(sum + " СЂСѓР±.");
	}

	if ($(".calc_block").length > 0) {
		reCalc();
	}

	$(".calc_block .usluga select").on("change", function () {
		reCalc();
	});

	$(".calc_block .razmer input").on("change", function () {
		reCalc();
	});

	$("#ch_gen").on("change", function () {
		reCalc();
	});

	$(".calc_block .viezd_block a").on("click", function () {
		$(".calc_block .viezd_block a").removeClass("active");
		$(this).addClass("active");
		reCalc();
	});

	$("#s1").on("change", function () {
		var id = $("#s1 option:selected").attr("data-id");
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: "/ajax/selects.php",
			data: {
				'id': id,
				'action': "get"
			},
			success: function (data) {
				$("#s2").html(data.data);
			}
		});
	});

	$(".calc_block .vid_rabot select").on("change", function () {
		var id = $(".calc_block .vid_rabot select option:selected").attr("data-id");
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: "/ajax/selects.php",
			data: {
				'id': id,
				'action': "get"
			},
			success: function (data) {
				$(".calc_block .usluga select").html(data.data);
			}
		});
	});

	$("#s2").on("change", function () {
		if ($("#s2 option:selected").val().length > 0)
			window.location.href = $("#s2 option:selected").data('url');
	});




	$(".calc_btn").draggable();

});
$(window).load(function () {
	$('#lightSlider').lightSlider({
		gallery: true,
		item: 1,
		loop: true,
		thumbItem: 5,
		slideMargin: 0,
		enableDrag: false,
		currentPagerPosition: 'left'
	});
});