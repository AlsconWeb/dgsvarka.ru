ymaps.ready(init);

function init() {
  var myMap = new ymaps.Map("map", {
    center: [maps_options.longitude, maps_options.latitude],
    zoom: 17,
    controls: []
  }, {
    searchControlProvider: 'yandex#search'
  });
  myGeoObject = new ymaps.GeoObject({
    geometry: {
      type: "Point",
      coordinates: [maps_options.longitude, maps_options.latitude]
    },
  }, {});
  myMap.behaviors
    .disable('scrollZoom')
  myMap.geoObjects
    .add(myGeoObject)
}