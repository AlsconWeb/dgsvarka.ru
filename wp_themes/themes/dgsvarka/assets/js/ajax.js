jQuery(document).ready(function ($) {
  $('#s1').change(function (e) {
    let data = {
      action: "post_list",
      termID: $(this).find(":selected").data('id')
    }
    $.ajax({
      url: urlAjax.url,
      type: "POST",
      data: data,
      success: function (res) {
        let response = JSON.parse(res)
        if (response.status == 200) {
          let list = response.list_title;
          let permalink = response.permalink;
          console.log("list", list, "perm", permalink);

          let select = $('#s2');
          $(select).find('option').remove();

          list.forEach((el, i) => {
            $(select).append('<option data-url="' + permalink[i] + '">' + el.toLowerCase() + '</option>');
          });

        } else {
          console.log("на сервере что то пошло не так *( ");
        }
      }
    });
  });
});