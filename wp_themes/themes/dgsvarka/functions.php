<?php

/**
 * Created 27.06.19
 * Version 1.0.0
 * Last update 
 * Author: Alex L
 */

/**
 * add_thmes_script
 * Version 1.0.0
 */
add_action('wp_enqueue_scripts', 'add_thmes_script');
function add_thmes_script()
{
  // add js
  wp_enqueue_script('jq_last', get_template_directory_uri() . '/assets/js/jq_last.js', array('jquery'), '', true);
  wp_enqueue_script('jq_ui', get_template_directory_uri() . '/assets/js/jquery-ui.js', array('jquery'), '', true);
  wp_enqueue_script('fancybox', get_template_directory_uri() . '/assets/js/jquery.fancybox.js', array('jquery'), '', true);
  wp_enqueue_script('validation', get_template_directory_uri() . '/assets/js/jquery.validate.min.js', array('jquery'), '', true);
  wp_enqueue_script('owl', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), '', true);
  wp_enqueue_script('maskedinput', get_template_directory_uri() . '/assets/js/maskedinput.js', array(), '', true);
  wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/common.js', array('jquery'), '', true);
  wp_enqueue_script('Ymap-js', 'https://api-maps.yandex.ru/2.1/?apikey=2ac3eb70-0ec0-4d7d-89d6-b7d3eabec2ec&amp;lang=ru_RU', array('main'), '1.0', true);
  wp_enqueue_script('map', get_template_directory_uri() . '/assets/js/map.js', array('Ymap-js'), '', true);

  wp_localize_script('map', 'maps_options', array(
    'longitude' => get_field('longitude', 'options'),
    'latitude' =>  get_field('latitude', 'options'),
  ));

  if (is_single()) {
    wp_enqueue_script('lightslider', get_template_directory_uri() . '/assets/js/lightslider.js', array('jquery'), '', true);
    wp_enqueue_script('ajax', get_template_directory_uri() . '/assets/js/ajax.js', array('main'), '', true);

    // Adds a variable with a URL for AJAX requests.
    wp_localize_script('ajax', 'urlAjax', array(
      'url' => admin_url('admin-ajax.php'),
    ));
  }

  //add css
  wp_enqueue_style("main_style", get_template_directory_uri() . "/assets/css/main.css", array(), "v.1.0.0");
  wp_enqueue_style("style", get_template_directory_uri() . "/style.css", array('main_style'), "v.1.0.0");
}

/**
 * init_theme
 * Version 1.0.0
 */
add_action('after_setup_theme', 'init_theme');
function init_theme()
{
  define('FS_METHOD', 'direct'); // not asked FTP 
  define('ALLOW_UNFILTERED_UPLOADS', true); // custom mime type
  require_once dirname(__FILE__) . '/lib/class-tgm-plugin-activation.php';
  add_action('tgmpa_register', 'register_required_plugins');

  // register menu
  register_nav_menus([
    'header_menu' => 'Меню в шапке',
  ]);
}

function widgets_inits()
{

  register_sidebar(array(
    'id'          => 'languish_menu',
    'name'        => 'languish',
    'description' => __('Your Widget Description.', 'text_domain'),
  ));
}
add_action('init', 'widgets_inits');

/**
 * register_required_plugins
 * Version 1.0.0
 */
function register_required_plugins()
{

  $plugins = array(

    array(
      'name' => 'ACF Pro',
      'slug' => 'advanced-custom-fields-pro-master',
      'source' => get_stylesheet_directory() . '/plugins/advanced-custom-fields-pro-master.zip',
      'required' => true,
    ),
    array(
      'name' => 'ACF table field',
      'slug' => 'advanced-custom-fields-table-field',
      'source' => get_stylesheet_directory() . '/plugins/advanced-custom-fields-table-field.zip',
      'required' => true,
    ),
    array(
      'name' => 'Cyr-To-Lat',
      'slug' => 'cyr2lat',
      'source' => get_stylesheet_directory() . '/plugins/cyr2lat.zip',
      'required' => true,
    ),
    array(
      'name' => 'Contact Form 7',
      'slug' => 'contact-form',
      'source' => get_stylesheet_directory() . '/plugins/contact-form.zip',
      'required' => true,
    ),
    array(
      'name' => 'Post Duplicator',
      'slug' => 'post-duplicator',
      'source' => get_stylesheet_directory() . '/plugins/post-duplicator.zip',
      'required' => true,
    ),
  );

  $theme_text_domain = 'dgsvarka';
  $config = array(
    'settings' => array(),
  );
  tgmpa($plugins, $config);
}

/**
 * mime_types 
 * add support svg file
 * Version 1.0.0
 */
function mime_types($mime_types)
{
  $mime_types['svg'] = 'image/svg+xml';
  return $mime_types;
}
add_filter('upload_mimes', 'mime_types', 1, 1);

// add custom post typ
require_once(dirname(__FILE__) . "/admin/add_CTP.php");

// add ajax
require_once(dirname(__FILE__) . "/admin/ajax.php");

// add theme Options
if (function_exists('acf_add_options_page')) {
  acf_add_options_page();
}
