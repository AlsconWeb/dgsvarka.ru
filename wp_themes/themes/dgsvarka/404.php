<?

/**
 * Created 19.07.19
 * Version 1.0.0
 * Last update 
 * Author: Alex L
 */

get_header();
?>
<section id="sblock">
  <div class="container">
    <div class="title"><?= $title; ?></div>
    <div class="left_text">
      <h1 class="title-404"> Oops... 404 Error </h1>
      <a href="<? bloginfo('url'); ?>" class="btn btn_yelow">Главная</a>
    </div>
  </div>
</section>
<? get_footer(); ?>