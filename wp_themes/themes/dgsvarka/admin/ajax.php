<?
/**
* Created 02.07.19
* Version 1.0.0
* Last update 
* Author: Alex L
*/

/**
 * @termID  term id from select date
 * 
 * We receive the list of articles for select "The name of the service "
 */

add_action('wp_ajax_post_list', 'post_list');
add_action('wp_ajax_nopriv_post_list', 'post_list');
function post_list(){
  $termID = $_POST['termID'];

  $list = new WP_Query( array(
    'tax_query' => array(
      array(
        'taxonomy' => 'type-of-work',
        'field'    => 'id',
        'terms'    => $termID 
      )
    )
  ) );

  $title = [];
  $permalink =[];
  foreach($list->posts as $val){
    $title[] = $val->post_title;
    $permalink[] = get_permalink($val->ID);
  }

  
  die(json_encode(array("status" => "200", "list_title" => $title , "permalink"=>$permalink)));
}
?>