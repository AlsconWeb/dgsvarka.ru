<?
/**
* Created 29.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
*/

add_action( 'init', 'register_post_types' );
function register_post_types(){
	register_post_type('services', array(
		'label'  => null,
		'labels' => array(
			'name'               => 'services', 
			'singular_name'      => 'service', 
			'add_new'            => 'Добавить Услугу',
			'add_new_item'       => 'Добавление Услги', 
			'edit_item'          => 'Редактирование Услугу', 
			'new_item'           => 'Новое Услуга', 
			'view_item'          => 'Смотреть Услугу', 
			'search_items'       => 'Искать Услугу', 
			'not_found'          => 'Не найдено', 
			'not_found_in_trash' => 'Не найдено в корзине', 
			'parent_item_colon'  => '', 
			'menu_name'          => 'Услуги', 
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true, 
		'exclude_from_search' => null, 
		'show_ui'             => true, 
		'show_in_menu'        => true, 
		'show_in_admin_bar'   => true, 
		'show_in_nav_menus'   => true, 
		'show_in_rest'        => false, 
		'rest_base'           => false, 
		'menu_position'       => 3,
		'menu_icon'           => 'dashicons-tickets-alt', 
		'hierarchical'        => true,
		'supports'            => array('title','editor', 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields'), 
		'taxonomies'          => array('type-of-work'),
		'has_archive'         => false,
		
	) );
}


add_action( 'init', 'create_taxonomy' );
function create_taxonomy(){

	register_taxonomy('type-of-work', array('services'), array(
		'label'                 => '', 
		'labels'                => array(
			'name'              => 'Тип работы',
			'singular_name'     => 'Тип работ',
			'search_items'      => 'Искать Тип работ',
			'all_items'         => 'Все Тип работ',
			'view_item '        => 'Просмотреть Тип работ',
			'parent_item'       => 'Родительский Тип работ',
			'parent_item_colon' => 'одительский Тип работ:',
			'edit_item'         => 'РедактироватьТип работ',
			'update_item'       => 'Обновить Тип работ',
			'add_new_item'      => 'Добавить Тип работ',
			'new_item_name'     => 'Новый Тип работ',
			'menu_name'         => 'Тип работ',
		),
		'description'           => '', 
		'public'                => true,
		'publicly_queryable'    => null, 
		'show_in_nav_menus'     => true, 
		'show_ui'               => true, 
		'show_in_menu'          => true, 
		'show_tagcloud'         => true, 
		'show_in_rest'          => false, 
		'rest_base'             => false, 
		'hierarchical'          => true,
		'rewrite'               => true,
		'capabilities'          => array(),
		'meta_box_cb'           => null,
		'show_admin_column'     => false, 
		'_builtin'              => false,
		'show_in_quick_edit'    => true, 
	) );
}


?>