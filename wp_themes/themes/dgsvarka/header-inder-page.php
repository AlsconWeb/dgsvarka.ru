<?

/**
 * Created 29.06.19
 * Version 1.1.0
 * Last update 30.07.19
 * Author: Alex L
 */
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<? bloginfo('template_url'); ?>/favicon.png">
  <title><? bloginfo('name'); ?> | <?= get_the_title(); ?></title>
  <link rel="dns-prefetch" href="//fonts.googleapis.com">
  <link href="https://fonts.gstatic.com" crossorigin="" rel="preconnect">
  <? wp_head(); ?>
</head>

<body>

  <? if (has_nav_menu('header_menu')) : ?>
    <section id="menu">
      <div class="container">
        <?
        if (has_nav_menu('header_menu')) {
          wp_nav_menu([
            'theme_location'  => 'header_menu',
            'menu'            => 'Menu',
            'container'       => '',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => 'manu_header',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => '',
          ]);
        } else { }
        ?>
      </div>
    </section>
  <? endif; ?>
  <?
  // var field ACF
  $logo = get_field('logo', 'options');
  $logo_text = get_field('logo_text', 'options');
  $time_work = get_field('time_work', 'options');
  $icon = get_field('icon', 'options');
  $text = get_field('text', 'options');
  $background = get_field('background', 'options');
  $on_filters = get_field('on_filters', 'options');

  // Filling for selects
  $tax = get_terms("type-of-work");
  $id = $post->ID;
  $terms = get_the_terms($id, 'type-of-work');

  $list = new WP_Query(array(
    'tax_query' => array(
      array(
        'taxonomy' => 'type-of-work',
        'field'    => 'id',
        'terms'    => $terms[0]->term_id
      )
    )
  ));
  ?>
  <header class="no-home" id="scrl1" style="background: url(<?= $background['url']; ?>) no-repeat center center;background-size: 100% auto;padding-bottom: 20px;">
    <div class="container">
      <div class="top">
        <div class="logo"><a href="<? bloginfo('url'); ?>"><img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>"><?= $logo_text; ?></a></div>
        <div class="ico_block">
          <p><?= $time_work; ?></p>
        </div>
        <? if ($text) : ?>
          <div class="phone_block"><?= $text; ?></div>
        <? endif; ?>
        <div class="menu-languish"><?php dynamic_sidebar('languish'); ?></div>
      </div>
    </div>
  </header>
  <? if ($on_filters) :
    ?>
    <section id="select_price">
      <div class="container">
        <div class="block1">
          <div class="title">Тип работ</div>
          <select id="s1">
            <? foreach ($tax as $val) : ?>
              <?= "val" . $val->name . "terms" .  $terms->name; ?>
              <? if ($val->name ==  $terms[0]->name) : ?>
                <option data-id="<?= $val->term_id; ?>" selected><?= $val->name; ?></option>
              <? else : ?>
                <option data-id="<?= $val->term_id; ?>"><?= $val->name; ?></option>
              <? endif; ?>
            <? endforeach; ?>
          </select>
        </div>
        <div class="block2">
          <div class="title">Название услуги</div>
          <select id="s2">
            <? foreach ($list->posts as $val) : ?>
              <? if ($val->ID == $id) : ?>
                <option data-url="<? the_permalink($val->ID); ?>" selected><?= mb_strtolower($val->post_title); ?></option>
              <? else : ?>
                <option data-url="<? the_permalink($val->ID); ?>"><?= mb_strtolower($val->post_title); ?></option>
              <? endif; ?>
            <? endforeach; ?>
          </select>
        </div>
      </div>
    </section>
  <? endif;
  ?>