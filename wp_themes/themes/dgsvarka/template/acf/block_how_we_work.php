<?

/**
 * Created 28.06.19
 * Version 1.0.1
 * Last update 23.07.19
 * Author: Alex L
 * Template Part name: block_how_we_work
 */
?>
<? if (get_row_layout() == 'block_how_we_work') : ?>
  <?
  // var field ACF
  $title = get_sub_field('title', true);
  $how_we_work = get_sub_field('how_we_work', true);
  $title_time_work = get_sub_field('title_time_work', true);
  $sub_title_time_work = get_sub_field('sub_title_time_work', true);
  $image = get_sub_field('image', true);
  $form = get_sub_field('form', true);
  ?>
  <section id="how">
    <div class="container">
      <div class="container">
        <div class="title"><?= $title; ?></div>
        <div class="items">
          <? foreach ($how_we_work as $key => $val) : ?>
            <div class="item">
              <div class="num"><?= $key + 1; ?></div>
              <div class="item_title"><?= $val['text'] ?></div>
            </div>
          <? endforeach; ?>
        </div>
        <div class="info" style="background: url(<?= $image['url']; ?>) no-repeat 100%">
          <div class="title2"><?= $title_time_work; ?></div>
          <div class="desc"><?= $sub_title_time_work; ?></div>
        </div>
        <div class="form">
          <?= do_shortcode($form); ?>
        </div>
      </div>
    </div>
  </section>
<? endif; ?>