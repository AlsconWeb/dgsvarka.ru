<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_gen
*/
?>
<? if (get_row_layout() == 'block_gen'):?>
<?
// var field ACF
$image = get_sub_field('image', true);
$title = get_sub_field('title', true);
$sub_title = get_sub_field('sub_title', true);
?>
<section id="gen">
  <div class="container">
    <div class="info">
      <div class="img"><img src="<?= $image['url'];?>" alt="<?= $image['alt'];?>"></div>
      <div class="text">
        <div class="title"><?= $title;?></div>
        <div class="desc"><?= $sub_title;?><br><br></div>
      </div>
    </div>
  </div>
</section>
<? endif;?>