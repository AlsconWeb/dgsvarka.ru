<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_our_team
*/
?>
<? if (get_row_layout() == 'block_our_team'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$text = get_sub_field('text', true);
?>
<section id="team">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="slider_arrows"><a class="prev"></a><a class="next"></a></div>
    <div class="items">
      <div class="bxslider_team owl-theme owl-carousel"></div>
    </div>
    <div class="desc"><?= $text;?></div>
  </div>
</section>
<? endif;?>