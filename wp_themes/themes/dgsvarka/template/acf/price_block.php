<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: price_block
*/
?>
<? if (get_row_layout() == 'price_block'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$price_menu = get_sub_field('price_menu', true);
$price_item_tables = get_sub_field('price_item_tables', true);
?>
<section id="scrl4">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="tabs_block">

      <div class="tabs_links">
        <ul>
          <? foreach($price_menu as $key => $val):?>
          <li><a data-id="<?= $key?>"><?= $val['item'];?></a></li>
          <? endforeach;?>
        </ul>
      </div>

      <div class="tabs_content">
        <? foreach($price_item_tables as $key => $val):?>
        <div class="tc" id="tc_<?= $key;?>">
        <?
        if ( ! empty ( $val ) ) {
          echo '<table border="0">';
          if ( ! empty( $val['table_item']['caption'] ) ) {
            echo '<caption>' . $val['caption'] . '</caption>';
          }
          if ( ! empty( $val['table_item']['header'] ) ) {
            echo '<thead>';
             echo '<tr>';
             foreach ( $val['table_item']['header'] as $key => $th ) {
               if($key > 0){
                echo '<td class="text-center">';
                echo $th['c'];
                echo '</td>';
               }else{
                 echo '<td >';
                 echo $th['c'];
                 echo '</td>';
               }
              }
              echo '</tr>';
              echo '</thead>';
          }
          echo '<tbody>';
          foreach ( $val['table_item']['body'] as $tr ) {
            echo '<tr class="b">';
            foreach ( $tr as $key => $td ) {
              if($key > 0){
                echo '<td class="text-center">';
                echo $td['c'];
                echo '</td>';
              }else{
                echo '<td>';
                echo $td['c'];
                echo '</td>';
              }
            }
          echo '</tr>';
          }
          echo '</tbody>';
          echo '</table>';
        }
        ?>
        </div>
        <? endforeach;?>
      </div>
    </div>
  </div>
</section>
<?endif;?>