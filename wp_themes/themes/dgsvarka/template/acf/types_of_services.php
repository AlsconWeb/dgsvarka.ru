<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: types_of_services
*/
?>
<?if (get_row_layout() == 'types_of_services'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$services_item = get_sub_field('services_item', true);
$text_after_block = get_sub_field('text_after_block', true);
$text_after_block_text = get_sub_field('text_after_block_text', true);

?>
<section id="scrl2">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="items">
     
      <? foreach($services_item as $item):?>
      <div class="item" style="background: url(<?= $item['image']['url'];?>) no-repeat center center; background-size: cover;">
        <div class="lbg"></div>
        
        <? foreach($item['services_item_type'] as $key => $val):?>
        <? if($key == 0):?>
        <div class="tt"><a href="<?= get_permalink($val);?>"><?= $val->post_title;?></a></div>
        <? else:?>
        <div class="i_item"><a href="<?= get_permalink($val);?>"><?= $val->post_title;?></a></div>
        <?endif;?>
        <?endforeach;?>
      </div>
      <? endforeach;?>
    </div>
    <div class="desc1"><?= $text_after_block;?></div>
    <div class="desc2"><?= $text_after_block_text;?></div>
  </div>
</section>
<?endif;?>