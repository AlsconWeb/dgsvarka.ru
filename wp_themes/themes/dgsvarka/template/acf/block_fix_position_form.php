<?

/**
 * Created 18.07.19
 * Version 1.0.0
 * Last update 
 * Author: Alex L
 * Template Part name: block_fix_position_form
 */
?>
<? if (get_row_layout() == 'block_fix_position_form') : ?>
  <?
  // var field ACF
  $text = get_sub_field('text', true);
  $short_code = get_sub_field('short_code', true);
  $color_bg_block = get_sub_field('color_bg_block', true);
  $color_text = get_sub_field('color_text', true);
  ?>
  <section class="fix_bottom" style="background:<?= $color_bg_block; ?>">
    <div class="container">
      <div class="text__block">
        <p style="color:<?= $color_text; ?>"><?= $text; ?></p>
      </div>
      <div class="form__block">
        <div class="form"><?= do_shortcode($short_code); ?></div>
      </div>
    </div>
  </section>
<? endif; ?>