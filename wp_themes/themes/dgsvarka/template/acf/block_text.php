<?
/**
* Created 30.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_text
*/
?>
<? if (get_row_layout() == 'block_text'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$text = get_sub_field('text', true);
?>
<section id="sblock">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="left_text">
      <?= $text;?>
    </div>
  </div>
</section>
<?else:?>
  <section id="sblock">
    <div class="container">
      <div class="title"><?= the_title();?></div>
      <div class="left_text">
        <?= the_comment();?>
      </div>
    </div>
  </section>
<? endif;?>