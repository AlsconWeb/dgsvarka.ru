<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_prices_of_image
*/
?>
<? if (get_row_layout() == 'block_prices_of_image'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$sub_title = get_sub_field('sub_title', true);
$form = get_sub_field('form', true);
$background = get_sub_field('background', true);
?>
 <section id="form3" style="background: url(<?= $background['url'];?>) no-repeat 50%;background-size: cover;">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="desc"><?= $sub_title;?></div>
    <div class="form">
      <?= do_shortcode($form);?>
    </div>
  </div>
</section>
<? endif;?>