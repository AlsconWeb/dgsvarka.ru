<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: grown_block
*/
?>
<? if (get_row_layout() == 'grown_block'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$grown_item = get_sub_field('grown_item', true);
$block_welding = get_sub_field('block_welding', true);
$weldings_item = get_sub_field('weldings_item', true);
?>
<section id="scrl3">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="items">
      <? foreach($grown_item as $item):?>
      <div class="item">
        <div class="img"><img src="<?= $item['image']['url'];?>" alt="<?= $item['image']['alt'];?>"></div>
        <div class="text"><?= $item['text'];?></div>
      </div>
      <? endforeach;?>
    </div>
    <div class="title2"><?= $block_welding['title'];?></div>
    <ul>
      <? foreach($weldings_item as $item):?>
      <li><?= $item['item'];?></li>
      <? endforeach;?>
    </ul>
  </div>
</section>
<? endif;?>