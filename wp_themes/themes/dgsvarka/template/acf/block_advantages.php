<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_advantages
*/
?>
<? if (get_row_layout() == 'block_advantages'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$advantages_item = get_sub_field('advantages_item', true);
?>
<section id="scrl5">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="items">
      <? foreach($advantages_item as $val):?>
      <div class="item">
        <div class="img">
          <img src="<?= $val['icons']['url'];?>" alt="<?= $val['icons']['alt'];?>">
        </div>
        <div class="item_title"><?= $val['title'];?></div>
        <div class="desc"><?= $val['text']?></div>
      </div>
      <? endforeach;?>
    </div>
  </div>
</section>
<? endif;?>