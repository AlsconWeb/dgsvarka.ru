<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_manufacture
*/
?>
<? if (get_row_layout() == 'block_manufacture'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$sub_title = get_sub_field('sub_title', true);
$image = get_sub_field('image', true);
?>
<section id="proiz">
  <div class="bg"></div>
  <div class="container">
    <div class="container">
      <div class="info">
        <div class="title"><?= $title;?></div>
        <div class="desc"></div>
        <div class="desc2"><?= $sub_title;?></div>
      </div>
    </div>
    <div class="img" style="background: url(<?= $image['url'];?>) no-repeat right top;background-size: cover;"></div>
  </div>
</section>
<? endif;?>