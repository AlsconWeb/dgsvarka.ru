<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_portfolio
*/
?>
<? if (get_row_layout() == 'block_portfolio'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$portfolio = get_sub_field('portfolio', true);
?>
<section id="scrl6">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="slider_arrows"><a class="prev"></a><a class="next"></a></div>
    <div class="items">
      <div class="bxslider_works owl-theme owl-carousel">
        <div class="owl2row-item">
          <? foreach($portfolio as $key => $val):?>
          <?if($key % 7 == 0 && $key != 0):?>
          </div>
          <div class="owl2row-item">
            <div class="item"><a class="fancy" href="<?= $val['image']['url'];?>" rel="gal"><img src="<?= $val['image']['url'];?>" alt="<?= $val['image']['alt'];?>"></a></div>
          <? else:?>
          <div class="item"><a class="fancy" href="<?= $val['image']['url'];?>" rel="gal"><img src="<?= $val['image']['url'];?>" alt="<?= $val['image']['alt'];?>"></a></div>
          <? endif;?>
          <? endforeach;?>
      </div>
    </div>
  </div>
</section>
<? endif;?>