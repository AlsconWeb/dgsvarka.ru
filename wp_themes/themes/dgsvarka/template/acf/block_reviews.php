<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_reviews
*/
?>
<? if (get_row_layout() == 'block_reviews'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$reviews = get_sub_field('reviews', true);
?>
<section id="scrl8">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="slider_arrows"><a class="prev"></a><a class="next"></a></div>
    <div class="items">
      <div class="bxslider_testimonials owl-theme owl-carousel">
        <? foreach($reviews as $val):?>
        <div class="item">
          <div class="review">
            <div class="review_title"><?= $val['title'];?></div>
            <div class="review_desc">
              <?= $val['text'];?>
            </div>
            <div class="open_close_desc">Читать полностью</div>
            <div class="img_block">
              <div class="audio">
                <div class="name"><?= $val['author_reviews'];?></div>
              </div>
            </div>
          </div>
        </div>
        <? endforeach;?>
      </div>
    </div>
  </div>
</section>
<? endif;?>