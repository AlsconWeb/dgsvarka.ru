<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_call_master
*/
?>
<? if (get_row_layout() == 'block_call_master'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$forma = get_sub_field('forma', true);
$icon = get_sub_field('icon', true);
$text = get_sub_field('text', true);
$adress_title = get_sub_field('adress_title', true);
$text_rep = get_sub_field('text_rep', true);
$link_to_whatsapp = get_sub_field('link_to_whatsapp', true);
?>
<section id="bot_contacts">
  <div class="container">
    <div class="hr"></div>
    <div class="title"><?= $title;?></div>
    <div class="form">
      <?= do_shortcode($forma);?>
    </div>
    <div class="ico_block"><a href="<?= $link_to_whatsapp?>"></a></div>
    <div class="phone_block"><?= $text?></div>
    <div class="address">
      <div class="address_title"><?= $adress_title;?></div>
      <? foreach($text_rep as $val):?>
      <div class="text"><?= $val['item_text'];?></div>
      <? endforeach;?>
    </div>
  </div>
</section>
<? endif;?>