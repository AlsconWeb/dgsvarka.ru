<?
/**
* Created 30.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: header_service
*/
?>
<? if (get_row_layout() == 'header_service'):?>
<?
// var field ACF
$price = get_sub_field('price', true);
$slider = get_sub_field('slider', true);
$text = get_sub_field('text', true);
$description_service = get_sub_field('description_service', true);

?>
<section id="main_desc">
  <div class="container">
    <div class="top_block">
      <div class="imgs_block">
        <ul id="lightSlider">
          <? foreach($slider as $val):?>
          <li data-thumb="<?= $val['image']['url'];?>" data-src="<?= $val['image']['url'];?>"><img src="<?= $val['image']['url'];?>"></li>
          <? endforeach;?>
        </ul>
      </div>
      <div class="desc_block">
        <div class="title">
          <h1><? the_title();?></h1>
        </div>
        <div class="price"><?= $price;?></div>
        <?= $text;?>
      </div>
    </div>
    <div class="desc_block">
      <?= $description_service;?>
    </div>
  </div>
</section>
<? endif;?>