<?

/**
 * Created 28.06.19
 * Version 1.1.0
 * Last update 30.07.19
 * Author: Alex L
 * Template Part name: header_main_banner
 */
?>
<? if (get_row_layout() == 'header_main_banner') : ?>
  <?
  // var field ACF
  $banner_image = get_sub_field('banner_image', true);
  $logo = get_sub_field('Logo', true);
  $logo_text = get_sub_field('Logo_text', true);
  $time_work = get_sub_field('time_work', true);
  $icon = get_sub_field('icon', true);
  $text_contact = get_sub_field('text_contact', true);
  $main_text = get_sub_field('main_text', true);
  $block_icon = get_sub_field('block_icon', true);
  $form_shortcode = get_sub_field('form_shordcode', true);
  ?>

  <header id="scrl1" style="background: url(<?= $banner_image['url']; ?>) no-repeat top center;background-size: cover;">
    <div class="container">
      <div class="top">
        <div class="logo"><a href="<? bloginfo('url'); ?>"><img src="<?= $logo['url']; ?>" alt=""><?= $logo_text; ?></a></div>
        <div class="ico_block">
          <p><?= $time_work; ?></p>
        </div>
        <div class="menu-languish"><?php dynamic_sidebar('languish'); ?></div>
        <div class="phone_block" style="margin-top:20px; background: url(<?= $icon['url']; ?>) no-repeat 0;"><?= $text_contact; ?></div>
      </div>
      <div class="mid">
        <div class="hr"></div>
        <div class="title">
          <div class="typed_texts">
            <h1 style="text-align: center;"><span style="font-family: tahoma, arial, helvetica, sans-serif;"><?= $main_text; ?></span></h1>
          </div>
          <div class="typed-off"></div>
        </div>
        <div class="items owl-theme owl-carousel">
          <? foreach ($block_icon as $item) : ?>
            <div class="item">
              <div class="img" style="max-width: 70%;margin-left: 25px;"><img src="<?= $item['icon']["url"]; ?>" alt="<?= $item['icon']["alt"] ?>"></div>
              <div class="text"><?= $item['text_icon_block'] ?></div>
            </div>
          <? endforeach; ?>
        </div>
        <div class="form">
          <?= do_shortcode($form_shortcode); ?>
        </div>
      </div>
      <div class="bot"><a id="scrl_b"><img src="<? bloginfo('template_url'); ?>/assets/img/scrl_b.png" alt=""></a></div>
    </div>
  </header>
<? endif; ?>