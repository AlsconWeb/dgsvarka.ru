<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_faq
*/
?>
<? if (get_row_layout() == 'block_faq'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$faq_item = get_sub_field('faq_item', true);
$btn_questions = get_sub_field('btn_questions', true);
?>
<section id="faq">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="faq_items">
      <? foreach($faq_item as $val):?>
      <div class="faq_item">
        <div class="ques"><?= $val['question'];?></div>
        <div class="answer">
          <p><?= $val['response'];?></p>
        </div>
      </div>
      <? endforeach;?>
    </div>
    <div class="mailto"><a href="<?= $btn_questions['link'];?>"><?= $btn_questions['text'];?></a></div>
  </div>
</section>
<? endif;?>