<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_master_form
*/
?>
<? if (get_row_layout() == 'block_master_form'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$sub_title = get_sub_field('sub_title', true);
$form_shordcode = get_sub_field('form_shordcode', true);
$image = get_sub_field('Image', true);

?>
<section id="form2">
  <div class="container">
    <div class="title"><?= $title;?></div>
    <div class="desc"><?= $sub_title;?></div>
    <div class="form">
      <?= $form_shordcode;?>
    </div>
    <div class="chel"><img src="<?= $image['url'];?>" alt="<?= $image['alt']?>"></div>
  </div>
</section>
<? endif;?>