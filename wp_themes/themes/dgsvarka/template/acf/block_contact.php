<?
/**
* Created 28.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
* Template Part name: block_contact
*/
?>
<? if (get_row_layout() == 'block_contact'):?>
<?
// var field ACF
$title = get_sub_field('title', true);
$text = get_sub_field('text', true);
$form = get_sub_field('form', true);
?>
<section id="scrl9">
  <div class="container">
    <div class="container">
      <div class="info">
        <div class="title"><?= $title;?></div>
        <div class="desc"><?= $text;?></div>
        <div class="form"><br>
          <?= do_shortcode($form);?>
        </div>
      </div>
    </div>
  </div>
  <div class="map">
    <div id="map">
      <!-- <script type="text/javascript" charset="utf-8" async="" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A76221acdba161032044cf74f2e4cc1adf2bf002c79099ff9e732cb095575eb11&width=100%&height=400lang=ru_RU&scroll=true"></script> -->
    </div>
  </div>
</section>
<? endif;?>