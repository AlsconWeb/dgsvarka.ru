<?

/**
 * Created 28.06.19
 * Version 1.0.0
 * Last update 
 * Author: Alex L
 * Template name: Home Page
 */
?>
<? get_header(); ?>
<? if (have_rows('cotent_template')) :

  // loop through the rows of data
  while (have_rows('cotent_template')) : the_row();
    switch (get_row_layout()) {
      case 'header_main_banner':
        get_template_part('template/acf/header_main_banner', 'header_main_banner');
        break;

      case 'types_of_services':
        get_template_part('template/acf/types_of_services', 'types_of_services');
        break;

      case 'grown_block':
        get_template_part('template/acf/grown_block', 'grown_block');
        break;

      case 'price_block':
        get_template_part('template/acf/price_block', 'price_block');
        break;

      case 'block_master_form':
        get_template_part('template/acf/block_master_form', 'block_master_form');
        break;

      case 'block_prices_of_image':
        get_template_part('template/acf/block_prices_of_image', 'block_prices_of_image');
        break;

      case 'block_gen':
        get_template_part('template/acf/block_gen', 'block_gen');
        break;

      case 'block_advantages':
        get_template_part('template/acf/block_advantages', 'block_advantages');
        break;

      case 'block_manufacture':
        get_template_part('template/acf/block_manufacture', 'block_manufacture');
        break;

      case 'block_how_we_work':
        get_template_part('template/acf/block_how_we_work', 'block_how_we_work');
        break;

      case 'block_portfolio':
        get_template_part('template/acf/block_portfolio', 'block_portfolio');
        break;

      case 'block_reviews':
        get_template_part('template/acf/block_reviews', 'block_reviews');
        break;

      case 'block_our_team':
        get_template_part('template/acf/block_our_team', 'block_our_team');
        break;

      case 'block_contact':
        get_template_part('template/acf/block_contact', 'block_contact');
        break;

      case 'block_faq';
        get_template_part('template/acf/block_faq', 'block_faq');
        break;

      case 'block_call_master':
        get_template_part('template/acf/block_call_master', 'block_call_master');
        break;

      case 'block_text':
        get_template_part('template/acf/block_text', 'block_text');
        break;

      case 'header_service':
        get_template_part('template/acf/header_service', 'header_service');
        break;

      case 'block_fix_position_form':
        get_template_part('template/acf/block_fix_position_form', 'block_fix_position_form');
        break;
    }

  endwhile;

else :

  get_template_part('template/acf/block_text', 'block_text');

endif;
?>

<? get_footer(); ?>