<?

/**
 * Created 27.06.19
 * Version 1.0.2
 * Last update 24.07.19
 * Author: Alex L
 */
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<? bloginfo('template_url'); ?>/favicon.png">
  <title><? bloginfo('name'); ?>| <? bloginfo('description'); ?></title>
  <link rel="dns-prefetch" href="//fonts.googleapis.com">
  <link href="https://fonts.gstatic.com" crossorigin="" rel="preconnect">
  <? wp_head(); ?>
</head>

<body>
  <? if (has_nav_menu('header_menu')) : ?>
    <section id="menu">
      <div class="container">
        <?
        if (has_nav_menu('header_menu')) {
          wp_nav_menu([
            'theme_location'  => 'header_menu',
            'menu'            => 'Menu',
            'container'       => '',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => 'manu_header',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => '',
          ]);
        } else { }
        ?>
      </div>
    </section>
  <? endif; ?>