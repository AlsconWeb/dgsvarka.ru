<? 
/**
* Created 27.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
*/
?>
<? get_header(); ?>
  <? get_template_part('template/acf/header_main_banner', 'header_main_banner');?>
  <? get_template_part('template/acf/types_of_services', 'types_of_services');?>
  <? get_template_part('template/acf/grown_block', 'grown_block');?>
  <? get_template_part('template/acf/price_block', 'price_block');?>
  <? get_template_part('template/acf/block_master_form', 'block_master_form');?>
  <? get_template_part('template/acf/block_prices_of_image', 'block_pricse_of_image');?>
  <? get_template_part('template/acf/block_gen', 'block_gen');?>
  <? get_template_part('template/acf/block_advantages', 'block_advantages');?>
  <? get_template_part('template/acf/block_manufacture', 'block_manufacture');?>
  <? get_template_part('template/acf/block_how_we_work', 'block_how_we_work');?>
  <? get_template_part('template/acf/block_portfolio', 'block_portfolio');?>
  <? get_template_part('template/acf/block_reviews', 'block_reviews');?>
  <? get_template_part('template/acf/block_our_team', 'block_our_team');?>
  <? get_template_part('template/acf/block_our_team', 'block_our_team');?>
  <? get_template_part('template/acf/block_contact', 'block_contact');?>
  <? get_template_part('template/acf/block_call_master', 'block_call_master');?>
<? get_footer();?>