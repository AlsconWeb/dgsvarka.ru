<?
/**
* Created 27.06.19
* Version 1.0.0
* Last update 
* Author: Alex L
*/
?>
<?
  // var field ACF
  $copyright = get_field('copyright', 'options');
  ?>
<footer> 
      <div class="copyright"><?= $copyright;?></div>
    </footer><a id="scroll-to-top" href="#" title="Scroll to Top">Top</a>
    <? wp_footer();?>
  </body>
</html>